<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:8|max:255',
            'confirm_password' => 'required|same:password|min:8|max:255'
        ]);

        $validatedData['password'] = bcrypt($validatedData['password']);
        $validatedData['confirm_password'] = bcrypt($validatedData['confirm_password']);


        // dd('berhasil');
        User::create($validatedData);

        return redirect('/')->with('success', 'Registrasi Berhasil! Silakan Login');
    }
}
