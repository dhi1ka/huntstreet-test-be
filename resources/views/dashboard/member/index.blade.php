@extends('dashboard.layouts.main')

@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h2>Daftar Member</h2>
</div>

@if (session()->has('success'))
  <div class="alert alert-success" role="alert">
    {{ session('success') }}
  </div>
@endif

<div class="table-responsive">
  <a href="/dashboard/member/create" class="btn btn-primary mb-3"><span data-feather="plus"></span> Buat Data Member</a>
  <table class="table table-striped table-sm">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Email</th>
        <th scope="col">Nama</th>
        <th scope="col">Tanggal Lahir</th>
        <th scope="col">Jenis Kelamin</th>
        <th scope="col">Designer Favorit</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($members as $member)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $member->email }}</td>
        <td>{{ $member->name }}</td>
        <td>{{ $member->date_of_birth }}</td>
        <td>{{ $member->gender }}</td>
        <td>{{ $member->designer_favorite }}</td>
        <td>
          <a href="/dashboard/member/products/{{ $member->id }}" class="badge bg-info"><span data-feather="eye"></span></a>
          <a href="/dashboard/member/products/{{ $member->id }}/edit" class="badge bg-warning"><span data-feather="edit"></span></a>
          <form action="/dashboard/member/products/{{ $member->id }}" class="d-inline" method="POST">
            @method('delete')
            @csrf
            <button class="badge bg-danger border-0" onclick="return confirm('Yakin?')"><span data-feather="x-circle"></span></button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection