<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Register from Link</title>
</head>
<body>
  <h1>Register from Link</h1>
  <form action="/register" method="POST">
    @csrf
    <label for="email">Email</label>
    <input type="email" name="email" id="email" disabled>
    <label for="name">Nama</label>
    <input type="text" name="name" id="name">
    <label for="date_of_birth">Tanggal Lahir</label>
    <input type="date" name="date_of_birth" id="date_of_birth">
    <label for="gender">Jenis Kelamin</label>
    <input type="text" name="gender" id="gender">
    <label for="designer_favorite">Designer Favorit</label>
    <select name="designer_favorite" id="designer_favorite">
      {{-- @foreach ($collection as $item)   --}}
        <option value=""></option>
      {{-- @endforeach --}}
    </select>
    <button type="submit">Submit</button>
  </form>
</body>
</html>