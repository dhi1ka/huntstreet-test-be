<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\MembersController;
use App\Http\Controllers\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::get('/register', [RegisterController::class, 'index'])->middleware('guest');

Route::post('/', [LoginController::class, 'authenticate']);
Route::post('/register', [RegisterController::class, 'store']);
Route::post('/logout', [LoginController::class, 'logout']);



Route::get('/getlink', function () {
    return view('getlink');
});


Route::get('/getcode', function () {
    return view('getcode');
});

Route::get('/dashboard', function () {
    return view('dashboard.index');
});

Route::get('/dashboard/member', function () {
    return view('dashboard.member.index');
});


Route::resource('/dashboard/member', MembersController::class)->middleware('auth');
